# CDAshop
  Fonctionnalités basiques :

  - [x] create components
  - [x] implementing google login
  - [x] display current user 
  - [x] using Async pipe
  - [x] extracting Service
  - [x] Protecting Routes
  - [x] Storing Users in database
  - [x] Hiding Admin links
  - [x] protecting Admin Routes
  - [x] Product Managment
    - [x] list categories from firebase
    - [x] save Products
    - [ ] form validation
    - [ ] add spectre card
    - [ ] rendering list of products
    - [ ] editing a Product
    - [ ] Updating a Product
    - [ ] Deleting a Product
    - [ ] Searching for Products

