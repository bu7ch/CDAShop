// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebase: {
    apiKey: "AIzaSyB-cypfrP5p1rn8hcmsumWb7c9pbmnbVio",
    authDomain: "cdashop-20ce9.firebaseapp.com",
    databaseURL: "https://cdashop-20ce9.firebaseio.com",
    projectId: "cdashop-20ce9",
    storageBucket: "cdashop-20ce9.appspot.com",
    messagingSenderId: "720424946803",
    appId: "1:720424946803:web:70d12c802216eefb826e06"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
